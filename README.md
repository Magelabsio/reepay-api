Reepay API
=============
Lightweight wrapper for the Reepay REST API.

Installation
------------

You can install reepay-api using Composer:

```
composer require magelabs/reepay-api
```